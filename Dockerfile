ARG NGINX_IMAGE_TAG=stable-alpine-for-hugo

FROM eclipsefdn/nginx:${NGINX_IMAGE_TAG}

ADD config/nginx /etc/nginx/conf.d
COPY public /usr/share/nginx/html/
