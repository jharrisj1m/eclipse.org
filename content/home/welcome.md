---
title: Getting to know Eclipse.org
date: 2022-01-06T19:39:43.000Z
description: ""
categories: []
keywords: []
slug: ""
toc: false
draft: false
lastmod: 2022-03-21T18:52:49.578Z
hide_sidebar: true
---

## What is Eclipse.org?

Eclipse is an open source community whose projects are focused on providing an extensible development platform and application frameworks for building software. Eclipse provides extensible tools and frameworks that span the software development lifecycle, including support for modeling, language development environments for Java, C/C++ and others, testing and performance, business intelligence, rich client applications and embedded development. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities and research institutions and individuals extend, complement and support the Eclipse Platform.

### Resources for the Eclipse Developer Community

*   [Eclipse Technical Documentation](https://help.eclipse.org/)
*   Project Overview Map with subprojects
*   Papers: [Eclipse Roadmap](https://www.eclipse.org/org/councils/roadmap_v4_0/index.php), [Whitepaper (.pdf)](https://outreach.eclipse.foundation/enabling-digital-transformation-white-paper)
*   [Peer-reviewed technical articles](https://wiki.eclipse.org/Eclipse_Corner)
*   [Advanced Search](/home/search/)
*   Catalogues of Eclipse related Events, Books, Tutorials, community websites

### The Eclipse Foundation

*   [Membership in the Eclipse Foundation](/org/membership/become_a_member/)
*   [List of Members](/org/membership/members/), Board of Directors, Technical Councils
*   [Legal and Licensing](/org/legal/)
*   Calendar of Events
*   [Press Releases](/org/press-release/) and case studies

### What is .. ?

*   [OSGi/RCP](/rcp/)
*   [SWT](/swt/)
*   [JDT](/jdt/)
*   [EMF](/emf/)
*   [Platform](/eclipse/)
*   [CDT](/cdt/)

### Eclipse Top Level Projects

*   [Eclipse (Platform)](/eclipse/)
*   [Business Intelligence and Reporting Tools (BIRT)](/birt/)
*   [Data Tools Platform (DTP)](/dtp/)
*   [Device Software Development Platform (DSDP)](/dsdp/)
*   [Technology (incubator)](/technology/)
*   [Test and Performance Tools Platform](/tptp/)
*   [Tools (C/C++, Modelling..)](/tools/)
*   [Web Tools Platform](/webtools/)


