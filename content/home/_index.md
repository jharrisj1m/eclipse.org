---
title: eclipse.org
headline: > 
  The Community for <span class="white">Open Collaboration</span>
  <br class="visible-md visible-lg"> and Innovation
tagline: The Eclipse Foundation provides our global community of individuals and
  organizations with a mature, scalable, and business-friendly environment for
  open source software collaboration and innovation.
custom_jumbotron_end: |
    <div class="custom-jumbotron-end-lead-text">
        The Eclipse Foundation is a community of
    </div>
    <div class="custom-jumbotron-end-link-group">
        <a class="link-unstyled" href="https://projects.eclipse.org/">
            <span class="link-overlay">Read More About Our Projects</span>
            Projects
        </a>
        <a class="link-unstyled" href="/collaborations/">
            <span class="link-overlay">Read More About Our Industry Collaborations</span>
            Collaborations
        </a>
        <a class="link-unstyled" href="/membership/">
            <span class="link-overlay">Become a Member</span>
            Members
        </a>
    </div>
hide_page_title: true
hide_sidebar: true
hide_breadcrumb: true
show_featured_story: true
date: 2004-02-02T18:54:43.927Z
layout: single
links:
  - - href: /org/
    - text: Learn More
container: container-fluid
lastmod: 2022-04-29T14:50:37.859Z
header_wrapper_class: header-default-bg-img
description: The Eclipse Foundation provides our global community of individuals and
  organizations with a mature, scalable, and business-friendly environment for
  open source software collaboration and innovation.
keywords:
  - eclipse
  - project
  - plug-ins
  - plugins
  - java
  - ide
  - swt
  - refactoring
  - free java ide
  - tools
  - platform
  - open source
  - development environment
  - development
  - ide
  - eclipse foundation
page_css_file: /public/css/homepage.css
---

{{< grid/section-container class="margin-top-40 margin-bottom-40" isMarkdown="false" >}}
    {{< pages/home/news title="Announcements" type="announcements" id="news-list-announcements" image_url="/images/redesign-stock/announcements.jpg" subscribe_url="https://newsroom.eclipse.org/rss/news/eclipse_org/announcements.xml" view_all_url="https://newsroom.eclipse.org/eclipse/announcements/" >}}
{{</ grid/section-container >}}

{{< grid/section-container class="margin-top-40 margin-bottom-60" isMarkdown="false" >}}
    {{< pages/home/news title="Community News" type="community_news" id="news-list-community" image_url="/images/redesign-stock/community-news.jpg" subscribe_url="https://newsroom.eclipse.org/rss/news/eclipse_org/community-news.xml" view_all_url="https://newsroom.eclipse.org/eclipse/community-news/">}}
{{</ grid/section-container >}}

{{< pages/home/newsroom_ads publishTo="eclipse_org_home" adFormat="ads_top_leaderboard" >}}

{{< pages/home/foundation-by-the-numbers >}}
