---
title: "SmartCLIDE"
date: 2020-01-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/smartclide.png"
tags: ["Cloud Computing", "IDE", "Deep-Learning", "Discovery", "Programming-by-Example"]
homepage: "https://smartclide.eu/"
facebook: ""
linkedin: "https://www.linkedin.com/company/smartclide/"
twitter: "http://twitter.com/SmartCLIDE"
youtube: "https://youtu.be/101djn48DS8"
funding_bodies: ["horizon2020"]
eclipse_projects: ["ecd.opensmartclide"]
project_topic: "Cloud Computing"
summary: "Smart Cloud IDE and Big Data Solutions"
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-projects-bg-img"
description: "# **SmartCLIDE**

The main objective of SmartCLIDE is to propose a radically new smart cloud-native development environment, based on the coding-by-demonstration principle, that will support creators of cloud services in the discovery, creation, composition, testing and deployment of full-stack data-centred services and applications in the cloud. SmartCLIDE will provide high levels of abstraction at all stages (development, testing, deployment and run-time) as well as self-discovery of IaaS and SaaS Services. SmartCLIDE will provide several categories of abstractions: at development stage, SmartCLIDE will provide abstractions on data transformations or processing; at testing stage, mechanisms to visualize flow and status or artefacts to automatically test the expected behaviour; at deployment stage, abstractions of physical and virtual resources; or at runtime, mechanisms to monitor the performance and operation of the service.


The cloud nature of the environment will enable collaboration between different stakeholders, and the self-discovery of IaaS and SaaS services and the high levels of abstraction will facilitate the composition and deployment of new services to nontechnical staff (with no previous experience on programming or on the administration of systems and infrastructure). Equally, hiding the complexity of the infrastructure, and adding intelligence to this layer, will allow to select the most adequate infrastructure services in each moment.
SmartCLIDE will allow SMEs and Public Administration to boost the adoption of Cloud and Big Data solutions, being validated at one solution oriented to Public Administration (Social Security System) and three different IoT and Big Data products of software development SMEs within the consortium.


This project is running from January 2020 - December 2022."
---

{{< grid/div class="container research-page-section">}}

[SmartCLIDE flyer](SmartCLIDE-Fact%20Sheet-01.pdf)

{{</ grid/div>}}

{{< grid/div class="bg-light-gray" isMarkdown="false">}}
{{< grid/div class="container research-page-section" >}}

# Consortium
* INSTITUT FÜR ANGEWANDTE SYSTEMTECHNIK BREMEN GMBH - Germany
* INTRASOFT INTERNATIONAL SA - Luxembourg
* Fundación Instituto Internacional de Investigación en Inteligencia Artificial y Ciencias de la Computación - Spain
* UNIVERSITY OF MACEDONIA - Greece
* ETHNIKO KENTRO EREVNAS KAI TECHNOLOGIKIS ANAPTYXIS - Greece
* X/OPEN COMPANY LIMITED - United Kingdom
* ECLIPSE FOUNDATION EUROPE GMBH - Germany
* WELLNESS TELECOM SL - Spain
* UNPARALLEL INNOVATION LDA - Portugal
* CONTACT SOFTWARE GMBH - Germany
* Kairos Digital Solutions - Spain

{{</ grid/div>}}
{{</ grid/div>}}
