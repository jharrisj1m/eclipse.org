#  Copyright (c) 2023 Eclipse Foundation, Inc.

#  This program and the accompanying materials are made available under the
#  terms of the Eclipse Public License v. 2.0 which is available at
#  http://www.eclipse.org/legal/epl-2.0.
#
#  Contributors:
#    Olivier Goulet <olivier.goulet@eclipse-foundation.org>
#
#  SPDX-License-Identifier: EPL-2.0

# Associate icons and text for appending to link types
link_types:
  eclipse:
    icon: fa fa-user
    postfix: (eclipse.org)
  eclipse_gitlab:
    icon: fa fa-gitlab
    postfix: (gitlab.eclipse.org)
  github:
    icon: fa fa-github
    postfix: (github.com)
  twitter:
    icon: fa fa-twitter
  mastodon:
    icon: fa fa-share-alt
  website:
    icon: fa fa-safari
  email: 
    icon: fa fa-envelope-o
  pgp:
    icon: fa fa-id-card-o
  linkedin:
    icon: fa fa-linkedin
    postfix: (LinkedIn)

# Security team members
staff:
  - name: Mikaël Barbero
    image: https://eclipse.org/org/foundation/images_staff/mikael.jpg
    links:
      - text: mbarbero
        type: eclipse
        url: https://accounts.eclipse.org/users/mbarbero
      - text: mbarbero
        type: eclipse_gitlab
        url: https://gitlab.eclipse.org/mbarbero
      - text: mbarbero
        type: github
        url: https://github.com/mbarbero
      - text: "@mikbarbero"
        type: twitter
        url: https://twitter.com/mikbarbero
      - text: "@mbarbero@hachyderm.io"
        type: mastodon
        url: https://hachyderm.io/@mbarbero
      - text: mikael.barbero.tech
        type: website
        url: https://mikael.barbero.tech
      - text: mikael.barbero@eclipse-foundation.org
        type: email
        url: mailto:mikael.barbero@eclipse-foundation.org
      - text: 3FAF B6A7 46AB 3E21 FDE0 E9E8 E1E6 5F0B 59A4 C4FB
        type: pgp
        url: https://keys.openpgp.org/search?q=3FAFB6A746AB3E21FDE0E9E8E1E65F0B59A4C4FB
  - name: Tiago Lucas
    image: https://www.eclipse.org/org/foundation/images_staff/tiago-lucas.jpg
    links:
      - text: tiagolucas
        type: eclipse
        url: https://eclipse.org/user/tiagolucas
      - text: tiagolucas
        type: eclipse_gitlab
        url: https://gitlab.eclipse.org/tiagolucas
      - text: TiagoLucas22478
        type: github
        url: https://github.com/TiagoLucas22478
      - text: tiagoslucas.medium.com
        type: website
        url: https://tiagoslucas.medium.com
      - text: tiago.lucas@eclipse-foundation.org
        type: email
        url: mailto:tiago.lucas@eclipse-foundation.org
  - name: Thomas Neidhart
    image: https://www.eclipse.org/org/foundation/images_staff/thomas-neidhart.png
    links:
      - text: netomi
        type: eclipse
        url: https://accounts.eclipse.org/users/netomi
      - text: netomi
        type: eclipse_gitlab
        url: https://gitlab.eclipse.org/netomi
      - text: netomi
        type: github
        url: https://github.com/netomi
      - text: netomi.github.io
        type: website
        url: https://netomi.github.io
      - text: thomas.neidhart@eclipse-foundation.org
        type: email
        url: mailto:thomas.neidhart@eclipse-foundation.org
  - name: Francisco Peréz
    image: https://www.eclipse.org/org/foundation/images_staff/francisco-perez.jpg
    links:
      - text: fcojperez
        type: eclipse
        url: https://accounts.eclipse.org/users/fcojperez
      - text: fcojperez
        type: eclipse_gitlab
        url: https://gitlab.eclipse.org/fcojperez
      - text: fperezel
        type: github
        url: https://github.com/fperezel
      - text: Francisco Peréz
        type: linkedin
        url: https://www.linkedin.com/in/fcojperez
      - text: francisco.perez@eclipse-foundation.org
        type: email
        url: mailto:francisco.perez@eclipse-foundation.org
  - name: Marta Rybczynska
    image: https://www.eclipse.org/org/foundation/images_staff/marta-rybczynska.jpg
    links:
      - text: mrybczyn
        type: eclipse
        url: https://accounts.eclipse.org/users/mrybczyn
      - text: mrybczyn
        type: eclipse_gitlab
        url: https://gitlab.eclipse.org/mrybczyn
      - text: mrybczyn
        type: github
        url: https://github.com/mrybczyn
      - text: marta.rybczynska@eclipse-foundation.org
        type: email
        url: mailto:marta.rybczynska@eclipse-foundation.org

# Other members on the ML
other:
  - name: Stewart Addison
    image: https://accounts.eclipse.org/sites/default/files/styles/site_login_profile_thumbnail/public/profile_pictures/picture-443079-1592562874.jpg
    links:
      - text: saddisonxls
        type: eclipse
        url: https://accounts.eclipse.org/users/saddisonxls
      - text: saddisonxls
        type: eclipse_gitlab
        url: https://gitlab.eclipse.org/saddisonxls
      - text: sxa
        type: github
        url: https://github.com/sxa
      - text: "@sxaTech"
        type: twitter
        url: https://www.twitter.com/sxaTech
  - name: Wayne Beaton
    image: https://www.eclipse.org/org/foundation/images_staff/wayne-beaton.jpg
    links:
      - text: wbeaton
        type: eclipse
        url: https://accounts.eclipse.org/users/wbeaton
      - text: wbeaton
        type: eclipse_gitlab
        url: https://gitlab.eclipse.org/wbeaton
      - text: waynebeaton
        type: github
        url: https://github.com/waynebeaton
      - text: "@waynebeaton"
        type: twitter
        url: https://www.twitter.com/waynebeaton
      - text: waynebeaton.wordpress.com
        type: website
        url: https://waynebeaton.wordpress.com/
  - name: Christopher Guindon
    image: https://www.eclipse.org/org/foundation/images_staff/chris.jpg
    links:
      - text: cguindon
        type: eclipse
        url: https://accounts.eclipse.org/users/cguindon
      - text: cguindon
        type: eclipse_gitlab
        url: https://gitlab.eclipse.org/cguindon
      - text: chrisguindon
        type: github
        url: https://github.com/chrisguindon
      - text: "@chrisguindon"
        type: twitter
        url: https://www.twitter.com/chrisguindon
      - text: chrisguindon.com
        type: website
        url: https://www.chrisguindon.com/
  - name: Alexander Kurtakov
    image: https://secure.gravatar.com/avatar/37ef89170bcdf23407f99586cb32343d.jpg
    links:
      - text: akurtakov
        type: eclipse
        url: https://accounts.eclipse.org/users/akurtakov
      - text: akurtakov
        type: eclipse_gitlab
        url: https://gitlab.eclipse.org/akurtakov
      - text: akurtakov
        type: github
        url: https://github.com/akurtakov
      - text: "@akurtakov"
        type: twitter
        url: https://www.twitter.com/akurtakov
      - text: akurtakov.blogspot.bg
        type: website
        url: https://akurtakov.blogspot.com/
  - name: Jesse McConnell
    image: https://secure.gravatar.com/avatar/5a3d9f0dcb7b133e92d2cd76e748702d.jpg
    links:
      - text: jmcconnell
        type: eclipse
        url: https://accounts.eclipse.org/users/jmcconnell
      - text: jmcconnell
        type: eclipse_gitlab
        url: https://gitlab.eclipse.org/jmcconnell
      - text: jmcc0nn3ll
        type: github
        url: https://github.com/jmcc0nn3ll
      - text: webtide.com
        type: website
        url: https://webtide.com/
  - name: Jens Reimann
    image: https://accounts.eclipse.org/sites/default/files/styles/site_login_profile_thumbnail/public/profile_pictures/picture-3834-1596011678.jpg
    links:
      - text: jreimann
        type: eclipse
        url: https://accounts.eclipse.org/users/jreimann
      - text: jreimann
        type: eclipse_gitlab
        url: https://gitlab.eclipse.org/jreimann
      - text: ctron
        type: github
        url: https://github.com/ctron
      - text: "@ctron"
        type: twitter
        url: https://www.twitter.com/ctron
      - text: dentrassi.de
        type: website
        url: https://dentrassi.de/
